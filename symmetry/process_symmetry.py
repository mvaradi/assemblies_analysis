import csv
from collections import defaultdict, OrderedDict 
import re

cyclic_sym_pattern = "^c\d{1,2}$"
dihedral_sym_pattern = "^d\d{1,2}$"

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def get_sym_operator(data, pattern):
    sym_operators = []
    for sym_op in data:
        if re.match(pattern, sym_op):
            sym_operators.append(sym_op)
    
    if len(sym_operators) == 1:
        return sym_operators[0]
    else:
        # sort strings with digit
        sym_operators.sort(key=natural_keys)
        # return the sym operator with the largest num
        return sym_operators[-1]

with open('symmetry_no_duplicates.csv') as f:
    symmetry_list = []
    csv_reader = csv.reader(f, delimiter = ',')
    next(csv_reader)
    for line in csv_reader:
        symmetry_list.append((line[0], line[1]))

d = defaultdict(list)
for elem in symmetry_list:
    d[elem[0]].append(elem[1])

symmetry_dict = OrderedDict()

for assembly, symmetry_list in d.items():
    if "i" in symmetry_list:
        symmetry_dict[assembly] = "i"
    elif "o" in symmetry_list:
        symmetry_dict[assembly] = "o"
    elif "t" in symmetry_list:
        symmetry_dict[assembly] = "t"
    elif any(re.match(dihedral_sym_pattern, sym_op) for sym_op in symmetry_list):
        symmetry_dict[assembly] = get_sym_operator(symmetry_list, dihedral_sym_pattern)
    elif any(re.match(cyclic_sym_pattern, sym_op) for sym_op in symmetry_list):
        symmetry_dict[assembly] = get_sym_operator(symmetry_list, cyclic_sym_pattern)
    else:
        symmetry_dict[assembly] = None

# field_names = ['assembly', 'symmetry_operator']
# with open('symmetry_reference.csv', 'w') as csvfile:
#     writer = csv.DictWriter(csvfile, fieldnames = field_names)
#     writer.writeheader()
#     writer.writerows(symmetry_dict)

with open('symmetry_reference.csv', 'w') as f:
    writer = csv.writer(f)
    writer.writerow(["Assembly_ID", "Sym_Operator"])
    for row in symmetry_dict.items():
        writer.writerow(row)